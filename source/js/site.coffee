$(document).on "ready", ->
  FieldDay.init()

FieldDay =

  onLoad: ->
    FieldDay.onResize()

  onResize: ->

  init: ->
    FieldDay.onLoad()
    $(window).resize ->
      FieldDay.onResize()

    $slider = $('.js-slider')
    $slider.on('cycle-initialized', (event, opts) ->
        $slider.addClass 'is-loaded'
    ).cycle
        fx: "fade"
        speed: 400
        slides: "> .slider-image"
        timeout: 0
        pager: "> .slider-pager-container .slider-pager"
        prev: "> .slider-previous"
        next: "> .slider-next"

    $(".js-testimonials_slider").cycle
        fx: "fade"
        speed: 400
        random: true
        slides: "> .testimonial"
        timeout: 0
        autoHeight: "calc"
        prev: "> .testimonials-slider-previous"
        next: "> .testimonials-slider-next"
        centerVert: true

    $('.js-gallery').magnificPopup
        type: 'inline'
        gallery: enabled: true

class FieldDay.Controller
